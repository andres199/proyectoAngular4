import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { verificarService } from "./verificar.service";

@Injectable()

export class protecRutasService implements CanActivate{
  loginState = false;
  constructor(private verificar: verificarService) {
    this.verificar.isOn()
      .subscribe((res) => {
        if (res && res.uid) {
          this.loginState = true;
        } else {
          this.loginState = false;
        }
      }, (error) => {
        this.loginState = false;
      })
  }
  canActivate(){
    return this.loginState;
  }
}