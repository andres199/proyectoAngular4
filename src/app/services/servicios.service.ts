import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import { Http, Headers } from "@angular/http";

@Injectable()

export class serviciosService{
    API_ENDPOINT = "https://proyectoangular4-fb16b.firebaseio.com";

    constructor(private aFireBase: AngularFireDatabase, private http: Http){

    }
    public getServicios(){
        return this.aFireBase.list('servicios/');
        //return this.http.get(this.API_ENDPOINT+'/servicios.json');
    }
    public getservicio(servicioId){
        return this.aFireBase.object('servicios/'+servicioId);
    }
    public addServicio(servicio){
        this.aFireBase.database.ref('servicios/'+servicio.id).set(servicio);
        //const headers = new Headers({"Content-Type":"application/json"});
        //return this.http.post(this.API_ENDPOINT+'/servicios.json', servicio, {headers: headers}).subscribe();
    }
    public getGeoData(direccion){
        return this.http.get('http://maps.google.com/maps/api/geocode/json?address='+direccion);
    }
}

