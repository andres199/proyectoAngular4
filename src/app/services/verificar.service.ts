import { Injectable } from "@angular/core";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Router } from "@angular/router";
import * as firebase from 'firebase/app';
import { Console } from "@angular/core/src/console";

@Injectable()

export class verificarService{
  estado: any ={};
  constructor(private angularFireAuth: AngularFireAuth, private aFirebase: AngularFireDatabase, private ruta: Router){

  }
  public facebookLogin(){
    this.angularFireAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then((res) => {
      alert('Login con facebook exitoso¡');
      this.ruta.navigate(['inicio']);
    })
    .catch((error) => {
      console.log(error);
      return error;
    })
  }
  public login(correo,contrasena){
    this.angularFireAuth.auth.signInWithEmailAndPassword(correo, contrasena)
      .then((respuesta) => {
        this.estado.status = true;
        this.estado.msg = respuesta;
        this.ruta.navigate(['inicio']);
      })
      .catch((error) => {
        this.estado.status = false;
        this.estado.msg = error;
      })
    return this.estado;
  }
  public registro(nombre,correo,contrasena){
    this.angularFireAuth.auth.createUserWithEmailAndPassword(correo,contrasena)
    .then((respuesta)=>{
      const id = this.angularFireAuth.auth.currentUser.uid;
      this.aFirebase.database.ref('users/'+id).set({nombre: nombre});
      this.estado.status=true;
      this.estado.msg=respuesta;
      this.ruta.navigate(['inicio']);
    })
    .catch((error)=>{
      this.estado.status=false;
      this.estado.msg=error;
    })
    return this.estado;
  }
  public isOn(){
    return this.angularFireAuth.authState;
  }
  public singOut(){
    this.angularFireAuth.auth.signOut();
    this.ruta.navigate(['inicio']);
  }
  public userInfo(){
    var datos: any = {};
    datos.email = this.angularFireAuth.auth.currentUser.email;
    datos.displayName = this.angularFireAuth.auth.currentUser.displayName;
    return datos;

  }
}