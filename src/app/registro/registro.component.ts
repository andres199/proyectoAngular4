import { Component } from '@angular/core';
import { verificarService } from '../services/verificar.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html'
})
export class registroComponent {
  datos: any = {};
  estado: any = {};
  constructor(private verificar: verificarService) {
  }
  registrar(){
    this.estado = this.verificar.registro(this.datos.nombre,this.datos.correo,this.datos.contrasena);
  }
}