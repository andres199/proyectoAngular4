import { Component } from '@angular/core';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html'
})
export class rolesComponent {
  roles:any = [
    {tipo: 'directa',categorie: 1, state: true, nombre:'Administrador'},
    {tipo: 'prestacion',categorie: 2, state: false, nombre:'Vendedor'},
    {tipo: 'directa',categorie: 3, state: true, nombre:'Bodeguero'},
    {tipo: 'directa',categorie: 3, state: false, nombre:'secretaria'},
    {tipo: 'directa',categorie: 3, state: true, nombre:'Auxiliar'}
  ];
  constructor(){
     
  }
  
}
