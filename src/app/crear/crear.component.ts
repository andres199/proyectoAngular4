import { Component } from '@angular/core';
import { serviciosService } from '../services/servicios.service';
import { ActivatedRoute } from '@angular/router';
import { debug } from 'util';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html'
})
export class crearComponent {
    servicio: any = {};
    servicioId: number = 0;
    accion: any ;
    private searchField: FormControl;
    resultsStream: Observable<any>;
    constructor(private servicios: serviciosService,private route: ActivatedRoute, private http: Http){
        this.servicioId = this.route.snapshot.params['id'];
        this.accion = this.route.snapshot.queryParams['action'];
        if(this.accion!='new'){
            this.servicios.getservicio(this.servicioId)
                .valueChanges().subscribe((servicio) => {
                    this.servicio = servicio;
                });
        }
        // search field para optener la direccion de los lugares introduciendo el nombre
        const URL = 'http://maps.google.com/maps/api/geocode/json?address=';
        this.searchField = new FormControl(); // se usa para escuchar lo que se typea
        this.resultsStream = this.searchField.valueChanges
        .debounceTime(500)
        .switchMap(query => this.http.get(URL + query)) // cada que se typea algo se añade la query ala url y se hace el llamado http
        .map(res => res.json()) // mapeamos a json para optener el objeto
        .map(res => res.results);
    }
    searchFieldGoogleMaps(){ // search field para optener la direccion de los lugares introduciendo el nombre
        const URL = 'http://maps.google.com/maps/api/geocode/json?address=';
        this.searchField = new FormControl(); // se usa para escuchar lo que se typea
        this.resultsStream = this.searchField.valueChanges
        .debounceTime(500)
        .switchMap(query => this.http.get(URL + query)) // cada que se typea algo se añade la query ala url y se hace el llamado http
        .map(res => res.json()) // mapeamos a json para optener el objeto
        .map(res => res.results);
    }
    crearServicio(){
        var direccion = this.servicio.calle + ',' + this.servicio.ciudad + ',' + this.servicio.pais;
        this.servicios.getGeoData(direccion)
        .subscribe((result) => {
            this.servicio.lat = result.json().results[0].geometry.location.lat;
            this.servicio.long = result.json().results[0].geometry.location.lng;
            if (this.accion != 'new'){
                this.servicio.id = this.servicioId;
                this.servicios.addServicio(this.servicio);
                alert('Servicio editado con exito');
            }else{
                this.servicio.id = Date.now();
                this.servicios.addServicio(this.servicio);
                alert('Servicio creado con exito');
            }

            this.servicio = {};
        });

    }
    getDir(result){
        this.servicio.calle = result.address_components[1].long_name + ' ' + result.address_components[0].long_name;
        this.servicio.ciudad = result.address_components[3].long_name;
        this.servicio.pais = result.address_components[5].long_name;
    }
}
