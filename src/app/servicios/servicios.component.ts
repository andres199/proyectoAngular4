import { Component } from '@angular/core';
import { serviciosService } from '../services/servicios.service';
import { protecRutasService } from '../services/protecRutas.service';
import { verificarService } from '../services/verificar.service';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html'
})
export class serviciosComponent {
    lat:number = 5.3183432;
    lng:number = -72.397751;
    services: any = [];
    error = false;
    errorMensaje = '';
    loginState = false;

    constructor(private servicios: serviciosService, private protectRutas: protecRutasService, private verificar: verificarService){
        this.verificar.isOn()
            .subscribe((res) => {
                if (res && res.uid) {
                    this.loginState = true;
                } else {
                    this.loginState = false;
                }
            }, (error) => {
                this.loginState = false;
            });
        this.servicios.getServicios()
        .valueChanges().subscribe((servicios) => {
            this.services = servicios;
           // var me = this;
           // this.services = Object.keys(me.services).map(function (key) { return me.services[key]; }); ** en caso que se use la opcion http toca convertir de objeto a array
            }, (error) => {
                console.log(error);
                this.error = true;
                this.errorMensaje = "En este momento tenemos incomvenientes, pedimos disculpas por las molestias.\nError: " + error.statusText;
            });
    }
}

