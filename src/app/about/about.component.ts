import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { serviciosService } from '../services/servicios.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html'
})
export class aboutComponent {
    servicioId: number = null;
    service: any = {};

    constructor(private route: ActivatedRoute, private servicios: serviciosService){
      this.servicioId = this.route.snapshot.params['id'];
      this.servicios.getservicio(this.servicioId)
      .valueChanges().subscribe((servicio) => {
        this.service = servicio;
      });
    }


}