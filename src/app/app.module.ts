import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";

//modulo de googleMaps
import { AgmCoreModule } from '@agm/core';
// directivas propia creada
import { resaltarDirective } from './directives/resaltar.directive';
//  se hace uso de host listeners
import { ContarClicksDirective } from './directives/contarClicks.directive';
//routes
import { Routes, RouterModule } from '@angular/router';
//componentes
import { AppComponent } from './app.component';
import { rolesComponent } from './roles/roles.component';
import { serviciosComponent } from './servicios/servicios.component';
import { aboutComponent } from './about/about.component';
import { crearComponent } from './crear/crear.component';
import { registroComponent } from './registro/registro.component';
import { loginComponent } from './login/login.component';
//services
import { serviciosService } from './services/servicios.service';
import { verificarService } from './services/verificar.service';
import { protecRutasService } from './services/protecRutas.service';
//fire base
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
//http
import { HttpModule } from '@angular/http';
//pipes
import { linkifyPipe } from './pipes/linkify.pipe';

const appRoutes: Routes = [
  {path: '', component: serviciosComponent},
  {path: 'inicio', component: serviciosComponent},
  {path: 'servicios', component: serviciosComponent},
  {path: 'roles', component: rolesComponent, canActivate: [protecRutasService]},
  {path: 'about/:id', component: aboutComponent},
  {path: 'crear/:id', component: crearComponent, canActivate: [protecRutasService]},
  {path: 'registro', component: registroComponent},
  {path: 'login', component: loginComponent}
];
//fire base
export const firebaseConfig = {
  apiKey: "AIzaSyC9mwpuTSOMwAZC0G54aqzNj5BTc74oipA",
  authDomain: "proyectoangular4-fb16b.firebaseapp.com",
  databaseURL: "https://proyectoangular4-fb16b.firebaseio.com",
  storageBucket: "proyectoangular4-fb16b.appspot.com",
  messagingSenderId: "180792067852"
};
@NgModule({
  declarations: [
    AppComponent,
    resaltarDirective,
    ContarClicksDirective,
    rolesComponent,
    serviciosComponent,
    aboutComponent,
    crearComponent,
    linkifyPipe,
    registroComponent,
    loginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA_waSD47_5xwkc51veYsTlNpXTFEzNRhE'
    }),
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpModule,
    ReactiveFormsModule
  ],
  providers: [
    serviciosService,
    verificarService,
    protecRutasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
