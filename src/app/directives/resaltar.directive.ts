import { Directive, Input, OnInit, ElementRef, Renderer2 } from "@angular/core";

@Directive({
   selector: '[resaltar]' 
})

export class resaltarDirective implements OnInit{
    constructor(private refElemento: ElementRef, private renderer: Renderer2){
     }
     @Input('resaltar') tipo: string = ''; 
    ngOnInit(){
       if(this.tipo == 'prestacion'){
        this.renderer.setStyle(this.refElemento.nativeElement,'background-color','gray'); 
       }
    }
} 