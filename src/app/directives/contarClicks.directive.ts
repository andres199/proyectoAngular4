import { Directive,  HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[contarClicks]'
})
export class ContarClicksDirective {
 clickCount = 0;
 @HostBinding('style.opacity') opacity : number = .1;
 @HostListener('click', ['$event.target']) onclick(btn){
  console.log( btn, "Cantidad de clicks: ", this.clickCount++);
  this.opacity += .1;
 }
}
