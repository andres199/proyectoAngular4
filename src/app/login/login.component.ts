import { Component } from '@angular/core';
import { verificarService } from '../services/verificar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class loginComponent {
  datos: any = {};
  estado: any ={};
  constructor(private verificar: verificarService) {
  }
  login(){
    this.estado = this.verificar.login(this.datos.correo,this.datos.contrasena);
  }
  facebookLogin(){
    this.verificar.facebookLogin();
  }

}