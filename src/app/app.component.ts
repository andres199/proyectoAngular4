import { Component } from '@angular/core';
import { verificarService } from './services/verificar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Working active !';
  datosUser: any = {};
  loginState = false;
  constructor(private verificar: verificarService){
    this.verificar.isOn()
    .subscribe((res) => {
      if(res && res.uid){
        this.loginState = true;
        this.datosUser = res;//obteniendo la informacion del usuario logeado
        /*
        tambien se puede obtener la informacion de el usuario haciendo uso de la siguiente funcion creada
        this.verificar.userInfo();
        */
      }else{
        this.loginState = false;
      }
    }, (error) => {
      this.loginState = false;
    })
  }
  singOut(){
    this.verificar.singOut();
  }
}
